var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    notifier = require('node-notifier'),
    path = require('path');

var errorHandler = function() {
    return plumber({
        errorHandler: function(err) {
            notifier.notify({
                title: err.plugin,
                message: err.message,
                icon: path.join(__dirname, 'gulp/icon.png')
            });
        }
    });
};

/*
    HTML
*/

var include = require('gulp-include'),
    minifyHTML = require('gulp-minify-html');

gulp.task('index', function() {
    return gulp.src('src/index.html')
        .pipe(errorHandler())
        .pipe(include())
        .pipe(minifyHTML())
        .pipe(gulp.dest('build/'));
});

/*
    JS
*/

var rename = require('gulp-rename'),
    webpack = require('gulp-webpack'),
    stripDebug = require('gulp-strip-debug'),
    streamify = require('gulp-streamify'),
    uglify = require('gulp-uglify');

gulp.task('js', function() {
    return gulp.src('src/js/index.js')
        .pipe(errorHandler())
        .pipe(webpack())
        .pipe(rename('index.js'))
        .pipe(gulp.dest('build/'))
        .pipe(stripDebug())
        .pipe(streamify(uglify()))
        .pipe(rename('index.min.js'))
        .pipe(gulp.dest('build/'));
});

/*
    LESS
*/

var less = require('gulp-less'),
    autoprefixer = require('gulp-autoprefixer'),
    cleancss = require('gulp-minify-css');

gulp.task('less', function() {
    return gulp.src('src/less/index.less')
        .pipe(errorHandler())
        .pipe(less())
        .pipe(autoprefixer(['last 3 versions', '>= 1%']))
        .pipe(cleancss({
            keepSpecialComments: 0,
            keepBreaks: false,
            benchmark: false,
            processImport: false,
            noRebase: true,
            noAdvanced: false,
            compatibility: false,
            debug: false
        }))
        .pipe(gulp.dest('build/'));
});

/*
    Images
*/

var changed = require('gulp-changed');

gulp.task('imgs', function() {
    gulp.src(['src/imgs/**/*.png', 'src/imgs/**/*.jpg'])
        .pipe(errorHandler())
        .pipe(changed('build/imgs/'))
        .pipe(gulp.dest('build/imgs/'));
});

/*
    Fonts
*/

gulp.task('font-files', function() {
    // TODO: zmień później na odp. rozszerzenie czcionek
    gulp.src(['src/fonts/**/*', 'src/fonts/**/*.ttf'])
        .pipe(errorHandler())
        .pipe(changed('build/fonts/'))
        .pipe(gulp.dest('build/fonts/'));
});

gulp.task('fonts-stylesheet', function() {
    gulp.src('src/fonts/fonts.less')
        .pipe(errorHandler())
        .pipe(less())
        .pipe(cleancss({
            keepSpecialComments: 0,
            keepBreaks: false,
            benchmark: false,
            processImport: false,
            noRebase: true,
            noAdvanced: false,
            compatibility: false,
            debug: false
        }))
        .pipe(gulp.dest('build/'));
});

gulp.task('fonts', ['font-files', 'fonts-stylesheet']);

/*
    General
*/

gulp.task('build', ['index', 'js', 'less', 'imgs', 'fonts']);

gulp.task('watch', function() {
    gulp.watch(['src/index.html', 'src/html/**/*.html'], ['index']);
    gulp.watch('src/js/**/*.js', ['js']);
    gulp.watch('src/less/**/*.less', ['less']);
    gulp.watch(['src/imgs/**/*.jpg', 'src/imgs/**/*.png'], ['imgs']);
    gulp.watch(['src/fonts/**/*', 'src/fonts/fonts.less'], ['fonts']); // TODO: zmień później * na odp. rozszerzenie czcionek
});

gulp.task('default', ['build', 'watch']);
