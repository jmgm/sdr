require('./lib/sprint');

var $window = $(window),
    $topMenu = $('.TopMenu');

function checkTopMenu() {
    if(window.scrollY > 520 ) {
        $topMenu.removeClass('_top');
    } else {
        $topMenu.addClass('_top');
    }
}

$window.on('scroll', checkTopMenu);
checkTopMenu();
